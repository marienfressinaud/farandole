# Farandole

Farandole est un générateur de sites statiques destinés à partager des sélections de films à voir.

## Contexte

Il m’est arrivé plusieurs fois ces derniers temps de vouloir partager une liste de films à voir à des amis. Notamment, je voulais suggérer des films sympas à voir pour des enfants en fonction de leur âge.

L’inspiration principale de Farandole est le site [filmspourenfants.net](https://www.filmspourenfants.net/). Pour l’instant, la tranche d’âge visée est les 4 - 6 ans.

## À propos du nom

Je sais pas, j’aime bien le mot « farandole ».

## Sélection personnelle

L’idée serait donc de partager ma liste personnelle de recommandations. Dans l’idéal, j’aurai vu tous les films listés dessus.

Farandole permettra à chacune et chacun (avec un minimum de compétences techniques dans l’immédiat) de créer son propre site / sa propre sélection. Un fichier `.gitlab-ci.yaml` permettra de facilement déployer un site Farandole sur Gitlab Pages.

## Fonctionnalités

- page principale : liste des films par âge et par ordre alphabétique
- lire les commentaires / avis de la personne qui partage sa liste
- filtrer la liste
	- avec une recherche libre
	- par âge
	- par tag

## Technique

Farandole sera écrit en Python. J’aime bien ce langage, je le trouve lisible, et j’ai déjà écrit plusieurs générateurs de sites statiques avec cette technologie.

Côté client, je vais avoir besoin d’un peu de JavaScript pour filtrer / rechercher. J’aimerais éviter une dépendance externe donc je pense écrire tout le JavaScript moi-même. L’éventuelle seule exception serait l’ajout de [Turbolinks](https://github.com/turbolinks/turbolinks) (et pas [Turbo](https://github.com/hotwired/turbo) qui fait plus de choses, mais dont je n’ai absolument pas besoin sur un site statique).

La liste des films sera générée à partir de fichiers Yaml placés dans un répertoire `movies/`. Chaque film sera contenu dans un fichier séparé. Exemple de format de fichier :

```yaml
---
name: "Astérix et Obélix : Mission Cléopâtre"
poster: /url/vers/l/affiche.jpg
year: 2002
time: 107
tags: ["Astérix", "Chabat", "farandole d’os"]
age: "8+"
comment: >
	J’aime beaucoup ce film. Voilà.
	À bientôt pour de nouvelles critiques sans concession.
...
```

Les commentaires pourront être écrits au format Markdown.

Concernant le format de l’âge, je ne prévois pour l’instant qu’un format "5+" pour signifier « à partir de 5 ans ». Je garde la porte ouverte à des évolutions du type "5-10" (« de 5 à 10 ans »).

Des images et fichiers statiques pourront être déposés dans un répertoire `static/`. Ils seront copiés tels quels.

## Maintenance et communauté

Je ne souhaite pas avoir à maintenir Farandole sur la durée. Je souhaite passer maximum 5 jours à travailler dessus. Une fois le site généré, je ne le ferai évoluer qu’à la marge.

Si des gens souhaitent faire évoluer Farandole ensuite, je leur donnerai les accès nécessaires.